// show date in sideBar
function showdate() {
    week= new Array("يكشنبه","دوشنبه","سه شنبه","چهارشنبه","پنج شنبه","جمعه","شنبه")
    months = new Array("فروردين","ارديبهشت","خرداد","تير","مرداد","شهريور","مهر","آبان","آذر","دی","بهمن","اسفند");
    a = new Date();
    d= a.getDay();
    day= a.getDate();
    month = a.getMonth()+1;
    year= a.getYear()-100;

	year = (year== 0)?2000:year;
	(year<1000)? (year += 2000):true;
    
	year -= ( (month < 3) || ((month == 3) && (day < 21)) )? 622:621;

	switch (month) {
    	case 1: (day<21)? (month=10, day+=10):(month=11, day-=20); break;
    	case 2: (day<20)? (month=11, day+=11):(month=12, day-=19); break;
    	case 3: (day<21)? (month=12, day+=9):(month=1, day-=20);   break;
    	case 4: (day<21)? (month=1, day+=11):(month=2, day-=20);   break;
    	case 5:
    	case 6: (day<22)? (month-=3, day+=10):(month-=2, day-=21); break;
    	case 7:
    	case 8:
    	case 9: (day<23)? (month-=3, day+=9):(month-=2, day-=22);  break;
    	case 10:(day<23)? (month=7, day+=8):(month=8, day-=22);    break;
    	case 11:
    	case 12:(day<22)? (month-=3, day+=9):(month-=2, day-=21);  break;
       default:  	break;
	}

    show = document.getElementById('date').innerHTML = (week[d]+" "+day+" "+months[month-1]+" "+ year);
}


// show hour in sidebar
function show_time(){
    d=new Date();
    H=d.getHours();H=(H<10)?"0"+H:H;
    i=d.getMinutes();i=(i<10)?"0"+i:i;
    s=d.getSeconds();s=(s<10)?"0"+s:s;
    document.getElementById('show_time').innerHTML=H+":"+i+":"+s;
    setTimeout("show_time()",1000);/* 1 sec */
} show_time();



// sticky sidebar filter in venues page
$(function() {
	$(".rightsidebar").stick_in_parent({
	  offset_top: 10,
	  recalc_every: 15,
	});
})
// sticky sidebar filter in venues page
$(function() {
	$(".leftsidebar").stick_in_parent({
	  offset_top: 10,
	  recalc_every: 15,
	});
})