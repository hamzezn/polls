from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class ActiveManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(active=True)


# Create your models here.
class BaseModel(models.Model):
    """
    a abstract model - it's preferred for all models to inheritance from this model
    """
    active = models.BooleanField(default=True, verbose_name="وضعیت فعال")

    objects = models.Manager()
    active_objects = ActiveManager()

    class Meta:
        abstract = True


class Poll(BaseModel):
    title = models.CharField(max_length=2000, verbose_name="سوال", )

    # date = models.DateField(verbose_name='تاریخ', default=datetime.date.today)

    class Meta:
        # ordering = ['-date']
        verbose_name = "سوال"
        verbose_name_plural = "سوالات"

    def __str__(self):
        return self.title

    @property
    def count_items(self):
        return self.item.all().count()

    def get_vote_count(self):
        return Vote.objects.filter(poll=self).count()

    vote_count = property(fget=get_vote_count)
    vote_count.fget.short_description = "تعداد شرکت کنندگان"

    @property
    def get_items(self):
        return self.item.all().values_list('pos', 'value')

    def can_vote(self, profile):
        return not self.vote_set.filter(profile=profile).exists()


class Item(BaseModel):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE, verbose_name="رای گیری", related_name="item")
    value = models.CharField(max_length=1000, verbose_name="گزینه", blank=False, null=False)
    pos = models.SmallIntegerField(default='0', verbose_name='جایگاه گزینه')

    class Meta:
        ordering = ['pos']
        verbose_name = "گزینه"
        verbose_name_plural = "گزینه ها"

    def __str__(self):
        return u'%s' % (self.value,)

    def get_vote_count(self):
        return Vote.objects.filter(item=self).count()

    vote_count = property(fget=get_vote_count)
    vote_count.fget.short_description = "تعداد رای "


GENDER_CHOICES = (
    (1, 'مرد'),
    (2, 'زن'),
)

Age_CHOICES = (
    (1, '۱۸-۲۸'),
    (2, '۲۹-۳۸'),
    (3, '۳۹-۴۸'),
    (4, '۴۹-۵۸'),
    (5, '۵۹ و بیشتر'),
)

EDUCATION_CHOICES = (
    (1, 'زیر دیپلم'),
    (2, 'دیپلم'),
    (3, 'کاردانی'),
    (4, 'کارشناسی'),
    (5, 'کارشناسی ارشد'),
    (6, 'دکترا'),
    (7, 'فوق دکترا'),
)

SALARY_CHOICES = (
    (1, 'کمتر از ۱ میلیون تومان'),
    (2, 'بین ۱ تا ۳ میلیون تومان'),
    (3, 'بین ۳ تا ۵ میلیون تومان'),
    (4, 'بیش از ۵ میلیون تومان'),
)
PROVINCE_CHOICES = (
    (1, 'آذربایجان شرقی'),
    (2, 'آذربایجان غربی'),
    (3, 'اردبیل'),
    (4, 'اصفهان'),
    (5, 'البرز'),
    (6, 'ایلام'),
    (7, 'بوشهر'),
    (8, 'تهران'),
    (9, 'چهارمحال و بختیاری'),
    (10, 'خراسان جنوبی'),
    (11, 'خراسان رضوی'),
    (12, 'خراسان شمالی'),
    (13, 'خوزستان'),
    (14, 'زنجان'),
    (15, 'سمنان'),
    (16, 'سیستان و بلوچستان'),
    (17, 'فارس'),
    (18, 'قزوین'),
    (19, 'قم'),
    (20, 'کردستان'),
    (21, 'کرمان'),
    (22, 'کرمانشاه'),
    (23, 'کهگیلویه و بویراحمد'),
    (24, 'گلستان'),
    (25, 'گیلان'),
    (26, 'لرستان'),
    (27, 'مازندران'),
    (28, 'مرکزی'),
    (29, 'هرمزگان'),
    (30, 'همدان'),
    (31, 'یزد'),
)

PARTICIPATION_CHOICES = (
    (1, 'بله'),
    (2, 'خیر'),
)


class Profile(models.Model):
    gender = models.PositiveSmallIntegerField(choices=GENDER_CHOICES, verbose_name="جنسیت")
    age = models.PositiveSmallIntegerField(choices=Age_CHOICES, verbose_name="سن")
    education = models.PositiveSmallIntegerField(choices=EDUCATION_CHOICES, verbose_name="تحصیلات")
    salary = models.PositiveSmallIntegerField(choices=SALARY_CHOICES, verbose_name="میزان در آمد")
    province = models.PositiveSmallIntegerField(choices=PROVINCE_CHOICES, verbose_name='استان')
    participation = models.PositiveSmallIntegerField(choices=PARTICIPATION_CHOICES, verbose_name="مشارکت در انتخابات")
    cookie = models.CharField(max_length=40, editable=False, db_index=True)

    def __str__(self):
        return self.cookie

    def get_profile_count(self):
        return Profile.objects.all().count()

    profile_count = property(fget=get_profile_count)
    profile_count.fget.short_description = "تعداد شرکت کنندگان "


class Vote(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE, verbose_name='نظرسنجی', db_index=True)
    item = models.ForeignKey(Item, on_delete=models.CASCADE, verbose_name='گزینه')
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, verbose_name='رای دهنده', db_index=True)
    datetime = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'نظر'
        verbose_name_plural = 'نظرات'
        # unique_together = [['profile', 'poll']]
        constraints = [
            models.UniqueConstraint(fields=['poll', 'profile'], name="profile-polled")
        ]

    def __str__(self):
        return 'رأی %s' % self.pk


class OtherFields(models.Model):
    page_header = models.CharField(max_length=500, blank=True, verbose_name="عنوان اصلی")
    left_header = models.CharField(max_length=500, blank=True, verbose_name="عنوان سمت چپ")
    image = models.ImageField(upload_to="images", blank=True, verbose_name="تصویر")
    tanks = models.CharField(max_length=500, blank=True, verbose_name="پیام عنوان نوار قرمز رنگ")
    left_panel = models.TextField(max_length=1000, blank=True, verbose_name="متن پنل سمت چپ")
    right_panel = models.TextField(max_length=1000, blank=True, verbose_name="متن پنل سمت راست")
    bottom = models.CharField(max_length=500, blank=True, verbose_name="نوشته ی پایین صفحه")
    end = models.TextField(max_length=1000, blank=True, verbose_name="متن پایان صفحه")

    class Meta:
        managed = True
        verbose_name = "اطلاعات مربوط به سایت"
        verbose_name_plural = 'اطلاعات مربوط به سایت'

    def __str__(self):
        return 'ویرایش اطلاعات سایت'

    def save(self):
        # count will have all of the objects from the Aboutus model
        count = OtherFields.objects.all().count()
        # this will check if the variable exist so we can update the existing ones
        save_permission = OtherFields.has_add_permission(self)

        # if there's more than two objects it will not save them in the database
        if count < 2:
            super(OtherFields, self).save()
        elif save_permission:
            super(OtherFields, self).save()

    def has_add_permission(self):
        return OtherFields.objects.filter(id=self.id).exists()
