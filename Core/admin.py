from django.contrib import admin
from django.contrib.auth.models import Group

from Core.models import Item, Poll, Vote, OtherFields


class PollItemInline(admin.TabularInline):
    model = Item
    extra = 0
    exclude = ['active']


class PollAdmin(admin.ModelAdmin):
    list_display = ('title', 'vote_count', 'active')
    list_editable = ('active',)
    inlines = [PollItemInline, ]


admin.site.register(Poll, PollAdmin)


class VoteAdmin(admin.ModelAdmin):
    change_list_template = 'admin_vote_list_template.html'
    list_display = ('profile', 'poll', 'item')
    list_filter = ('poll',)


@admin.register(OtherFields)
class Other(admin.ModelAdmin):
    pass


admin.site.register(Vote, VoteAdmin)
admin.site.site_header = 'سایت'
admin.site.unregister(Group)
