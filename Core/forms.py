from django import forms
from django.forms import RadioSelect

from Core.models import Profile, GENDER_CHOICES, Age_CHOICES, EDUCATION_CHOICES, SALARY_CHOICES, PARTICIPATION_CHOICES


class QuestionForm(forms.Form):
    def __init__(self, question, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        choice_list = [x for x in question.get_items()]
        self.fields["value"] = forms.ChoiceField(choices=choice_list, widget=RadioSelect)


class ProfileForm(forms.ModelForm):
    gender = forms.ChoiceField(choices=GENDER_CHOICES, widget=RadioSelect, label="جنسیت")
    age = forms.ChoiceField(choices=Age_CHOICES, widget=RadioSelect, label="سن")
    education = forms.ChoiceField(choices=EDUCATION_CHOICES, widget=RadioSelect, label="تحصیلات")
    salary = forms.ChoiceField(choices=SALARY_CHOICES, widget=RadioSelect, label="میزان در آمد")
    participation = forms.ChoiceField(choices=PARTICIPATION_CHOICES, widget=RadioSelect, label="در انتخابات شرکت می کنید؟")

    class Meta:
        model = Profile
        exclude = ('id',)
