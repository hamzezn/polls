from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [
    # path('', views.Home.as_view(), name='home'),
    path('', views.PrePoll.as_view(), name='prepoll'),
    path('active_polls/', views.AllPolls.as_view(), name='all_polls'),
    url(r'^vote/(?P<poll_pk>\d+)/$', views.vote, name='poll_ajax_vote'),
    # url(r'^poll/(?P<poll_pk>\d+)/$', views.poll, name='poll'),
    path('end/', views.End.as_view(), name='end'),
    path('export/', views.ExportResult.as_view(), name='export_result'),
]
