# Generated by Django 3.0.2 on 2020-01-26 09:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Core', '0003_otherfields'),
    ]

    operations = [
        migrations.AddField(
            model_name='otherfields',
            name='left_header',
            field=models.CharField(default='11111111', max_length=500, verbose_name='عنوان سمت چپ'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='otherfields',
            name='page_header',
            field=models.CharField(max_length=500, verbose_name='عنوان اصلی'),
        ),
        migrations.AlterField(
            model_name='otherfields',
            name='tanks',
            field=models.CharField(max_length=500, verbose_name='پیام تشکر عنوان'),
        ),
    ]
