import ast

import requests
import xlwt
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.crypto import get_random_string
from django.views.generic import FormView, TemplateView

from Core.forms import ProfileForm
from Poll import settings
from .models import Poll, Item, Vote, Profile
from .utils import set_cookie


# class Home(TemplateView):
#     template_name = 'poll/home.html'


def vote(request, poll_pk):
    if request.is_ajax():
        try:
            get_poll = Poll.objects.get(pk=poll_pk)
        except:
            return HttpResponse('Wrong parameters', status=400)
        item_pk = request.GET.get("item", False)
        if not item_pk:
            return HttpResponse('Wrong parameters', status=400)

        try:
            item = Item.objects.get(pk=item_pk)
        except:
            return HttpResponse('Wrong parameters', status=400)

        accept_key = request.COOKIES.get('accept_key', None)
        if accept_key:
            profile = Profile.objects.get(cookie=accept_key)
            if profile:
                try:
                    Vote.objects.create(poll=get_poll, profile=profile, item=item, )
                except:
                    temp = Vote.objects.filter(poll=get_poll, profile=profile).first()
                    Vote.objects.filter(pk=temp.id).update(item=item)

                response = HttpResponse(status=200)
                old_poll = request.COOKIES.get('polls', None)
                if not old_poll:
                    set_poll = {poll_pk}
                else:
                    set_poll = ast.literal_eval(old_poll)  # str to set
                    set_poll.add(poll_pk)

                set_cookie(response, 'polls', set_poll, 100)  # add current poll and expire 100 days later

            else:
                response = redirect("end")
            return response

        else:
            profile = None
            print("................... not valid user ......................")

    return HttpResponse(status=400)


def poll(request, poll_pk):
    try:
        poll = Poll.objects.get(pk=poll_pk)
    except Poll.DoesNotExists:
        return HttpResponse('Wrong parameters', status=400)

    items = Item.objects.filter(poll=poll)

    return render(request, "poll/poll.html", {
        'poll': poll,
        'items': items,
    })


class End(TemplateView):
    template_name = 'poll/end.html'


class PrePoll(FormView):
    template_name = 'poll/prepoll.html'
    form_class = ProfileForm
    extra_context = {'site_key': settings.RECAPTCHA_SITE_KEY, }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.object = None

    def generate_key(self):
        """ User otp key generator """
        key = get_random_string(length=32)
        if self.is_unique():
            return key
        self.generate_key()

    def is_unique(self):
        try:
            Profile.objects.get(cookie=self)
        except Profile.DoesNotExist:
            return True
        return False

    def form_valid(self, form):
        request = self.request
        user = form.save()
        unique_key = self.generate_key()
        user.cookie = unique_key
        user.save()
        response = redirect('all_polls')
        response.set_cookie('accept_key', unique_key, max_age=30 * 24 * 60 * 60)  # max age is 20 day
        set_cookie(response, 'polls', {0}, 100)  # init poll and expire 100 days later
        messages.success(request, 'سپاس گزاریمُ، آیا مایل به شرکت در مرحله دوم نظرسنجی هستید؟')
        return response

    # def form_invalid(self, form):
    #     """If the form is invalid, render the invalid form."""
    #     request = self.request
    #     return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        data = request.POST
        secret_key = settings.RECAPTCHA_SECRET_KEY
        data = {
            'response': data.get('g-recaptcha-response'),
            'secret': secret_key
        }
        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()

        if result_json.get('success'):
            form = self.get_form()
            accept_key = request.COOKIES.get('accept_key', None)
            if not accept_key:
                return self.form_valid(form)
            elif accept_key:
                try:
                    profile = Profile.objects.get(cookie=accept_key)
                except:
                    profile = None
                if not profile:
                    return self.form_valid(form)
                else:
                    return redirect('all_polls')
        else:
            return self.render_to_response({'is_robot': True})

    def get(self, request, *args, **kwargs):
        have_accept_key = request.COOKIES.get('accept_key', None)
        print(have_accept_key)
        if have_accept_key:
            try:
                profile = Profile.objects.get(cookie=have_accept_key)
            except:
                profile = None
            if profile:
                response = redirect("all_polls")
            else:
                response = self.render_to_response(self.get_context_data())
        else:
            response = self.render_to_response(self.get_context_data())
        return response


class AllPolls(TemplateView):
    template_name = 'poll/active_polls.html'

    def get(self, request, *args, **kwargs):
        have_accept_key = request.COOKIES.get('accept_key', None)
        old_poll = request.COOKIES.get('polls', None)

        if old_poll:
            poll = ast.literal_eval(old_poll)  # str to set
            context = {'Active_Polls': Poll.active_objects.exclude(id__in=poll), }
        else:
            context = {'Active_Polls': Poll.active_objects.all(), }
        if have_accept_key:
            try:
                profile = Profile.objects.get(cookie=have_accept_key)
            except:
                profile = None
            if profile:
                response = self.render_to_response(context)
            else:
                response = redirect("prepoll")
        else:
            response = redirect("prepoll")
        return response


class ExportResult(TemplateView, LoginRequiredMixin):
    def get(self, request, *args, **kwargs):
        # def export_votes_xls(request):
        if request.user.is_authenticated:
            response = HttpResponse(content_type='application/ms-excel')
            response['Content-Disposition'] = 'attachment; filename="Votes.xls"'

            wb = xlwt.Workbook(encoding='utf-8')
            ws = wb.add_sheet('Votes')

            # Sheet header, first row
            row_num = 0

            font_style = xlwt.XFStyle()
            font_style.font.bold = True

            columns = ['شناسه کاربر', 'جنسیت', 'سن', 'تحصیلات', 'در آمد', 'استان', 'شرکت در انتخابات', ]

            polls = Poll.active_objects.all()
            questions = polls.values_list('title')
            for i in questions:
                columns.append(i)

            for col_num in range(len(columns)):
                ws.write(row_num, col_num, columns[col_num], font_style)

            # Sheet body, remaining rows
            font_style = xlwt.XFStyle()
            rows = []
            all_profiles = Profile.objects.all()
            for profile in all_profiles:
                row = list(Profile.objects.filter(id=profile.id).values_list('id', 'gender', 'age', 'education', 'salary', 'province', 'participation', ).get())
                votes_profile = []
                for x in polls:
                    a_vote = Vote.objects.filter(poll_id=x, profile_id=profile.id).values_list('item__pos', flat=True)
                    if a_vote:
                        votes_profile += a_vote
                    else:
                        votes_profile.append(None)
                row += votes_profile
                rows.append(row)

            for row in rows:
                row_num += 1
                for col_num in range(len(row)):
                    ws.write(row_num, col_num, row[col_num], font_style)

            wb.save(response)
        else:
            response = redirect("prepoll")
        return response
